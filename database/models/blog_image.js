'use strict';
module.exports = (sequelize, DataTypes) => {
  const BlogImage = sequelize.define('blog_image', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    blog_id: {
      type: DataTypes.INTEGER
    },
    image_id: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true
  });
  BlogImage.associate = function (models) {
    // associations can be defined here
  };
  return BlogImage;
};