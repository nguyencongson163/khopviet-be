'use strict';
module.exports = (sequelize, DataTypes) => {
  const category = sequelize.define('category', {
    id: {
      allowNull: false,
      type: DataTypes.INTEGER,
      primarykey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING
    },
    priority: {
      type: DataTypes.STRING
    },
    meta_title: {
      type: DataTypes.STRING
    },
    meta_description: {
      type: DataTypes.STRING
    },
    keyword: {
      type: DataTypes.STRING
    },
    state: {
      type: DataTypes.STRING
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true
  });
  category.associate = function(models) {
    // associations can be defined here
  };
  return category;
};